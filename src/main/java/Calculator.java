import java.util.ArrayList;

public class Calculator {
    CalculatorService service = null;
    public Calculator(CalculatorService service){
    this.service = service;
    }
    public int add(int i, int j){
        System.out.println("Addiere "+i+" und "+j);
        return i+j;
    }

    public int subtrahieren(int i , int j){
        return i-j;
    }

    public int komplex(int i, int j){
        return (service.call(i, j))*i;
    }
}
