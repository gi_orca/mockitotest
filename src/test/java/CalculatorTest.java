import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.*;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.mockito.Mockito.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class CalculatorTest {
    @Mock
    CalculatorService mockedService;
    private Calculator c= null;

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Before
    public void setup(){

        this.c = new Calculator(mockedService);
    }

    @After
    public void cleanup(){
        this.c = null;
    }
    @Test
    public void testAddieren(){
        assertEquals(4, c.add(1,3));
        assertEquals(0, c.add(-1,1));
    }

    @Test
    public void testSubtrahieren(){

        assertEquals(0,c.subtrahieren( 3, 3));
        assertEquals(-1,c.subtrahieren( 3, 4));
    }

    @Test
    public void testKomplex(){
        when(mockedService.call(2,1)).thenReturn(3);
        assertEquals(6, c.komplex(2, 1));
        verify(mockedService).call(2,1);
        when(mockedService.call(0,2)).thenReturn(2);
        assertEquals(0, c.komplex(0,2));
        verify(mockedService).call(0,2);
    }
}
